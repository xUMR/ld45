using System.Collections;
using TMPro;
using UnityEngine;

public abstract class Tutorial : MonoBehaviour
{
    [SerializeField] protected TMP_Text tutorialText;
    protected bool DestructionBegun;

    protected IEnumerator DestroyCoroutine()
    {
        DestructionBegun = true;

        var t = 0f;
        var runUntil = Time.time + 1;
        while (Time.time < runUntil)
        {
            tutorialText.alpha = Mathf.Lerp(1, 0, Helpers.EaseOutCubic(t));
            t += Time.deltaTime;
            yield return null;
        }

        tutorialText.alpha = 0;
        Destroy(gameObject, 1);
    }
}
