public static class Helpers
{
    public static readonly float Tolerance = 1E-10f;

    public static readonly string AsciiLetters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static readonly string AsciiLettersUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static readonly string AsciiLettersLower = "abcdefghijklmnopqrstuvwxyz";

    // assume t is in [0, 1]
    public static float EaseOutCubic(float t) => 1 - (1 - t) * (1 - t) * (1 - t);
}
