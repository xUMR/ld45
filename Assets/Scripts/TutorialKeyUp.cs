using UnityEngine;

public sealed class TutorialKeyUp : Tutorial
{
    [SerializeField] private KeyCode key;

    private bool _listenForKeyUp;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!DestructionBegun && other.CompareTag(Tags.Player))
        {
            _listenForKeyUp = true;
        }
    }

    private void Update()
    {
        if (DestructionBegun) return;
        if (!_listenForKeyUp) return;

        if (Input.GetKeyUp(key))
        {
            StartCoroutine(DestroyCoroutine());
        }
    }
}
