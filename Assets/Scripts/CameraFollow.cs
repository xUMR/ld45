using System;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float speed;
    private Vector3 _offset;

    private Vector3 _velocity;

    private void Awake()
    {
        _offset = transform.position;
    }

    private void LateUpdate()
    {
        var targetX = target.position.x;

        var position = transform.position;
        var nextPosition = position.WithX(targetX);

//        var newPosition = Vector3.Lerp(transform.position, nextPosition, speed);
        var newPosition = Vector3.SmoothDamp(position, nextPosition, ref _velocity, speed);

        transform.position = newPosition;
    }
}
