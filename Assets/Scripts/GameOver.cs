using UnityEngine;

public class GameOver : MonoBehaviour
{
    [SerializeField] private string gameOverScene;

    private void OnEnable()
    {
        Events.OnGameOver += HandleGameOver;
    }

    private void OnDisable()
    {
        Events.OnGameOver -= HandleGameOver;
    }

    private void HandleGameOver()
    {
        Events.ChangeScene(gameOverScene);
    }
}
