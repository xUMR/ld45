using System;
using UnityEngine;

public static class Inputs
{
    public enum KeyState : byte
    {
        None,
        KeyHeld,
        KeyUp,
        KeyDown
    }

    public struct ArrowKeys
    {
        public readonly KeyState Up;
        public readonly KeyState Down;
        public readonly KeyState Left;
        public readonly KeyState Right;

        public ArrowKeys(KeyState up = KeyState.None,
            KeyState down = KeyState.None,
            KeyState left = KeyState.None,
            KeyState right = KeyState.None)
        {
            Up = up;
            Down = down;
            Left = left;
            Right = right;
        }
    }

    private static ArrowKeys _arrowKeysState;
    private static float _lastUpdateTime;

    public static KeyState GetKeyState(KeyCode keyCode)
    {
        if (Input.GetKeyDown(keyCode)) return KeyState.KeyDown;
        if (Input.GetKey(keyCode)) return KeyState.KeyHeld;
        if (Input.GetKeyUp(keyCode)) return KeyState.KeyUp;

        return KeyState.None;
    }

    public static ArrowKeys GetArrowKeysState()
    {
        if (Mathf.Abs(Time.realtimeSinceStartup - _lastUpdateTime) < Helpers.Tolerance) return _arrowKeysState;

        var up = GetKeyState(KeyCode.UpArrow);
        var down = GetKeyState(KeyCode.DownArrow);
        var left = GetKeyState(KeyCode.LeftArrow);
        var right = GetKeyState(KeyCode.RightArrow);

        _lastUpdateTime = Time.realtimeSinceStartup;

        _arrowKeysState = new ArrowKeys(up, down, left, right);
        return _arrowKeysState;
    }
}

public static class Ext
{
    public static bool IsHeld(this Inputs.KeyState ks) => ks == Inputs.KeyState.KeyHeld;
    public static bool IsDown(this Inputs.KeyState ks) => ks == Inputs.KeyState.KeyDown;
    public static bool IsUp(this Inputs.KeyState ks) => ks == Inputs.KeyState.KeyUp;
}
