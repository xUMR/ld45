using System;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    [SerializeField] private float force;
    [SerializeField] private Rigidbody2D body;
    [SerializeField] private GroundCheck groundCheck;
    [SerializeField] private PlayerState playerState;

    private bool _jumpRequested;

    private void Update()
    {
        if (playerState.IsCrouching) return;

        _jumpRequested = Inputs.GetArrowKeysState().Up.IsDown();
    }

    private void FixedUpdate()
    {
        if (!groundCheck.IsOnGround) return;

        if (!_jumpRequested) return;
        _jumpRequested = false;

        body.AddForce(force * Vector2.up, ForceMode2D.Impulse);
    }
}
