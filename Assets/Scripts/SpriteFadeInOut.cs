using System.Collections;
using UnityEngine;

public class SpriteFadeInOut : MonoBehaviour
{
    [SerializeField] private new SpriteRenderer renderer;
    [SerializeField] private float durationIn;
    [SerializeField] private float durationOut;
    private float _stepIn;
    private float _stepOut;

    private void Awake()
    {
        renderer.color = renderer.color.WithA(0);
        StartCoroutine(FadeInOutCoroutine());

        _stepIn = 1 / durationIn;
        _stepOut = 1 / durationOut;
    }

    private IEnumerator FadeInOutCoroutine()
    {
        while (renderer.color.a < 1)
        {
            renderer.color = renderer.color.WithA(renderer.color.a + _stepIn * Time.deltaTime);
            yield return null;
        }

        renderer.color = renderer.color.WithA(1);

        while (renderer.color.a > 0)
        {
            renderer.color = renderer.color.WithA(renderer.color.a - _stepOut * Time.deltaTime);
            yield return null;
        }

        renderer.color = renderer.color.WithA(0);
    }
}
