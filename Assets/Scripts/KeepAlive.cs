using System;
using UnityEngine;

public class KeepAlive : MonoBehaviour
{
    private static KeepAlive _instance;

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        DontDestroyOnLoad(gameObject);
    }
}
