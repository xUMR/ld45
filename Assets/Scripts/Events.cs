using System;
using UnityEngine;

public static class Events
{
    public static event Action<bool> OnCrouch;
    public static void Crouch(bool crouch) => OnCrouch?.Invoke(crouch);

    public static event Action<Spell, SpellDirection> OnSpellCast;
    public static void CastSpell(Spell spell, SpellDirection direction) => OnSpellCast?.Invoke(spell, direction);

    public static event Action<string, float> OnChat;
    public static void TriggerChat(string message, float duration) => OnChat?.Invoke(message, duration);

    public static event Action<Vector3> OnParticlesRequested;
    public static void RequestParticles(Vector3 at) => OnParticlesRequested?.Invoke(at);

    public static event Action<string> OnSceneChangeRequested;
    public static void ChangeScene(string scene) => OnSceneChangeRequested?.Invoke(scene);

    public static event Action OnGameOver;
    public static void GameOver() => OnGameOver?.Invoke();
}
