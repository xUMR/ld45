using System;
using UnityEngine;

public class PlayerState : MonoBehaviour
{
    [SerializeField] private bool isJumping;
    [SerializeField] private bool isCrouching;

    public bool IsJumping => isJumping;
    public bool IsCrouching => isCrouching;

    private void OnEnable()
    {
        Events.OnCrouch += HandleCrouch;
    }

    private void OnDisable()
    {
        Events.OnCrouch -= HandleCrouch;
    }

    private void HandleCrouch(bool crouch)
    {
        isCrouching = crouch;
    }
}
