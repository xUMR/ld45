using UnityEngine;

public static class WaitFor
{
    public static readonly WaitForFixedUpdate FixedUpdate = new WaitForFixedUpdate();
}
