using System;
using UnityEngine;

public class PlayerCrouch : MonoBehaviour
{
    private struct OffsetSize2d
    {
        public readonly Vector2 Offset;
        public readonly Vector2 Size;

        public OffsetSize2d(BoxCollider2D collider) : this(collider.offset, collider.size) { }

        public OffsetSize2d(Vector2 offset, Vector2 size)
        {
            Offset = offset;
            Size = size;
        }

        public void UpdateGivenCollider(BoxCollider2D collider)
        {
            collider.offset = Offset;
            collider.size = Size;
        }
    }

    [SerializeField] private GroundCheck groundCheck;
    [SerializeField] private new BoxCollider2D collider;

    [SerializeField] private AnimationClip[] crouchAnimations;
    [SerializeField] private new Animation animation;

    private OffsetSize2d _initialOffsetSize2d;
    private OffsetSize2d _crouchOffsetSize2d;

    private bool _isCrouching;
    private bool _crouchRequested;
    private bool _standRequested;

    private void UpdateCollider(OffsetSize2d offsetSize2d) => offsetSize2d.UpdateGivenCollider(collider);

    private void PlayAnimationClip(int index)
    {
        animation.clip = crouchAnimations[index];
        animation.Play();
    }

    private void Awake()
    {
        _initialOffsetSize2d = new OffsetSize2d(collider);
        _crouchOffsetSize2d = new OffsetSize2d(_crouchOffsetSize2d.Offset, _crouchOffsetSize2d.Size);
    }

    private void Update()
    {
        if (!groundCheck.IsOnGround) return;

        var down = Inputs.GetArrowKeysState().Down;
        _crouchRequested = down.IsDown();
        _standRequested = down.IsUp();

        if (_isCrouching && _standRequested)
        {
            _isCrouching = false;
            PlayAnimationClip(1);
            Events.Crouch(_isCrouching);
        }

        if (_crouchRequested)
        {
            _isCrouching = true;
            PlayAnimationClip(0);
            Events.Crouch(_isCrouching);
        }
    }

//    private void FixedUpdate()
//    {
//        if (!groundCheck.IsOnGround) return;
//
//        if (_crouchRequested)
//        {
//            UpdateCollider(_crouchOffsetSize2d);
//        }
//        else if (_standRequested)
//        {
//            UpdateCollider(_initialOffsetSize2d);
//        }
//    }
}
