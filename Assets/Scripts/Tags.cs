public static class Tags
{
    public static readonly string Player = "Player";
    public static readonly string Follower = "Follower";
    public static readonly string Damage = "Damage";
    public static readonly string NoFollowZone = "NoFollowZone";
    public static readonly string Enemy = "Enemy";
}
