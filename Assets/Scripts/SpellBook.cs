using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SpellBook : MonoBehaviour
{
    [SerializeField] private AnimationClip[] clips;
    [SerializeField] private new Animation animation;
    [SerializeField] private Spell[] spells;
    [SerializeField] private TMP_Text[] texts;
    [SerializeField] private CanvasGroup castCanvasGroup;

    private Spell _selectedSpell;

    private bool _isCastingSpell;
    private bool _readyToCast;
    private int _keyIndex; // key = arrow key for spell casting

    private void OnEnable()
    {
        Events.OnCrouch += HandleCrouch;

        for (var i = 0; i < spells.Length; i++)
        {
            var spell = spells[i];
            var text = texts[i];

            var keyIndex0 = KeyCodeToArrowSpriteIndex(spell.Combo[0]);
            var keyIndex1 = KeyCodeToArrowSpriteIndex(spell.Combo[1]);

            text.text = $"<sprite={keyIndex0}> <sprite={keyIndex1}> {spell.SpellName}";
        }
    }

    private void OnDisable()
    {
        Events.OnCrouch -= HandleCrouch;
    }

    private void HandleCrouch(bool crouched)
    {
        _isCastingSpell = crouched;

        animation.clip = crouched ? clips[0] : clips[1];
        animation.Play();

        if (!crouched)
            ResetSpellCasting();
    }

    private static int KeyCodeToArrowSpriteIndex(KeyCode arrowKeyCode)
    {
        switch (arrowKeyCode)
        {
            case KeyCode.UpArrow: return 0;
            case KeyCode.DownArrow: return 1;
            case KeyCode.LeftArrow: return 2;
            case KeyCode.RightArrow: return 3;
            default: throw new ArgumentException();
        }
    }

    private static SpellDirection KeyCodeToSpellDirection(KeyCode arrowKeyCode)
    {
        switch (arrowKeyCode)
        {
            case KeyCode.UpArrow: return SpellDirection.Area;
            case KeyCode.LeftArrow: return SpellDirection.Left;
            case KeyCode.RightArrow: return SpellDirection.Right;
            default: throw new ArgumentOutOfRangeException(nameof(arrowKeyCode));
        }
    }

    private void Update()
    {
        if (!_isCastingSpell) return;

        if (_readyToCast && Mathf.Abs(castCanvasGroup.alpha - 1) > Helpers.Tolerance)
        {
            castCanvasGroup.alpha = 1;
        }

        if (!Input.anyKeyDown) return;

        var arrowKeys = Inputs.GetArrowKeysState();
        var lastArrowKey =
            arrowKeys.Left.IsDown() ? KeyCode.LeftArrow :
            arrowKeys.Up.IsDown() ? KeyCode.UpArrow :
            arrowKeys.Right.IsDown() ? KeyCode.RightArrow :
            default;

        if (lastArrowKey == default)
        {
            Debug.LogWarning("This shouldn't have happened!");
            return;
        }

        if (_keyIndex < 2)
        {
            for (var i = 0; i < spells.Length; i++)
            {
                var spell = spells[i];
                if (spell.Combo[_keyIndex] != lastArrowKey)
                {
                    texts[i].alpha = .32f;
                }
                else if (_keyIndex == 1 && texts[i].alpha > .99f)
                {
                    _selectedSpell = spell;
                }
            }

            _keyIndex = (_keyIndex + 1) % 3;

            if (_keyIndex == 2 && !ReferenceEquals(_selectedSpell, null))
            {
                _readyToCast = true;
                castCanvasGroup.alpha = 1;
            }
            else if (_keyIndex == 2 && ReferenceEquals(_selectedSpell, null))
            {
                ResetSpellCasting();
            }
        }
        else
        {
            var direction = KeyCodeToSpellDirection(lastArrowKey);
            Events.CastSpell(_selectedSpell, direction);

            ResetSpellCasting();
        }
    }

    private void ResetSpellCasting()
    {
        foreach (var tmpText in texts)
        {
            tmpText.alpha = 1;
        }

        _readyToCast = false;
        castCanvasGroup.alpha = .32f;
        _keyIndex = 0;
        _selectedSpell = null;
    }
}
