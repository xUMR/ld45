using UnityEngine;

public class Mover : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Vector2 direction;
    [SerializeField] private Rigidbody2D body;

    private void FixedUpdate()
    {
        body.velocity = speed * direction;
    }

    public void Reverse()
    {
        direction = -direction;
    }

    public void SendUp()
    {
        direction = Vector2.up;
//        velocity = new Vector2(0, Mathf.Abs(velocity.x));
    }

    public void SetDirection(Vector2 newDirection)
    {
        direction = newDirection;
    }
}
