using UnityEngine;

public sealed class TutorialTriggerExit : Tutorial
{
    private void OnTriggerExit2D(Collider2D other)
    {
        if (!DestructionBegun && other.CompareTag(Tags.Player))
        {
            StartCoroutine(DestroyCoroutine());
        }
    }
}
