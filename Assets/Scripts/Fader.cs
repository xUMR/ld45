using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private float delay;
    [SerializeField] private float duration;

    [SerializeField] private bool playOnAwake;

    private void OnEnable()
    {
        Events.OnSceneChangeRequested += HandleSceneChangeRequest;

        if (playOnAwake)
        {
            StartCoroutine(FadeCoroutine(1, 0));
        }
    }

    private void OnDisable()
    {
        Events.OnSceneChangeRequested -= HandleSceneChangeRequest;
    }

    private void HandleSceneChangeRequest(string scene)
    {
        StartCoroutine(FadeCoroutine(0, 1));
        StartCoroutine(ChangeSceneCoroutine(scene));
    }

    private IEnumerator ChangeSceneCoroutine(string scene)
    {
        var waitUntil = Time.time + delay + duration;
        while (Time.time < waitUntil)
        {
            yield return null;
        }

        GC.Collect();

        SceneManager.LoadScene(scene);
    }

    private IEnumerator FadeCoroutine(float from, float to)
    {
        var waitUntil = Time.time + delay;
        while (Time.time < waitUntil)
        {
            yield return null;
        }

        image.enabled = true;

        var t = 0f;
        while (t < 1)
        {
            var newAlpha = Mathf.Lerp(from, to, Helpers.EaseOutCubic(t));
            image.color = image.color.WithA(newAlpha);
            t += Time.deltaTime / duration;

            yield return null;
        }

        image.color = image.color.WithA(to);
        if (Mathf.Abs(to - 0) < Helpers.Tolerance)
        {
            image.enabled = false;
        }
    }
}
