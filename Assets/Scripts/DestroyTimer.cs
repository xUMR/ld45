using UnityEngine;

public class DestroyTimer : MonoBehaviour
{
    [SerializeField] private float countdown;

    private void Start()
    {
        Destroy(gameObject, countdown);
    }
}
