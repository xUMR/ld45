using System;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
//    [SerializeField] private EnemyData template;
    [SerializeField] private float hp;
    [SerializeField] private new ParticleSystem particleSystem;

    private void OnEnable()
    {
//        hp = template.hp;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag(Tags.Damage)) return;

        var damage = other.gameObject.GetComponent<Damage>();
        var amount = damage.Amount;
        DealDamage(amount);

//        Destroy(damage.gameObject);
    }

    public void DealDamage(float amount)
    {
        particleSystem.Play();
        hp -= amount;
        if (hp < Helpers.Tolerance)
        {
            // todo death sequence
            // StartCoroutine(template.Death());
            Destroy(gameObject, .05f);
        }
    }
}
