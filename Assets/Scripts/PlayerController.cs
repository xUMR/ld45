using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D body;
    [SerializeField] private PlayerState playerState;

    private void FixedUpdate()
    {
        if (playerState.IsCrouching) return;

        var input = Inputs.GetArrowKeysState();
        var movement = 0f;
        if (input.Left.IsHeld())
        {
            movement -= 1;
        }
        if (input.Right.IsHeld())
        {
            movement += 1;
        }

        body.velocity = body.velocity.WithX(movement * speed);
    }

    private void Update()
    {
        UpdateOrientation();
    }

    private void UpdateOrientation()
    {
        var input = Inputs.GetArrowKeysState();
        if (input.Left.IsDown())
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        if (input.Right.IsDown())
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag(Tags.Enemy))
        {
            Events.GameOver();
        }
    }
}
