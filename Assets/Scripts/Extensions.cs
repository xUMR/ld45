using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static Vector3 WithX(this Vector3 v, float x) => new Vector3(x, v.y, v.z);
    public static Vector2 WithX(this Vector2 v, float x) => new Vector3(x, v.y);

    public static Vector3 AddX(this Vector3 v, float x) => new Vector3(v.x + x, v.y, v.z);
    public static Vector3 AddY(this Vector3 v, float y) => new Vector3(v.x, v.y + y, v.z);
    public static Vector3 AddXY(this Vector3 v, float x, float y) => new Vector3(v.x + x, v.y + y, v.z);

    public static Vector2 XY(this  Vector3 v) => new Vector2(v.x, v.y);

     public static Color WithA(this Color c, float a) => new Color(c.r, c.g, c.b, a);

     public static Rect WithWidth(this Rect r, float width) => new Rect(r.x, r.y, width, r.height);

     public static T Random<T>(this IEnumerable<T> source)
     {
         var current = default(T);
         var count = 1.0;

         foreach (var item in source)
         {
             if (UnityEngine.Random.value < 1 / count)
             {
                 current = item;
             }
             count++;
         }

         return current;
     }
}
