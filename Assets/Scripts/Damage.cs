using System;
using UnityEngine;

public class Damage : MonoBehaviour
{
    [SerializeField] private float amount;

    public float Amount => amount;
}
