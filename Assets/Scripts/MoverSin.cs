using System;
using UnityEngine;

public class MoverSin : MonoBehaviour
{
    [SerializeField] private Vector3 movement;
    [SerializeField] private float frequency;
    [SerializeField] private float magnitude;
    [SerializeField] private float offset;

    [SerializeField] private float spawnTime;

    private void OnEnable()
    {
        spawnTime = Time.time;
    }

    private void Update()
    {
        var sin = Mathf.Sin(Time.time * frequency + spawnTime + offset + Mathf.PI / 2) * magnitude;
        var translation = movement * sin;
        transform.position += translation;
    }
}
