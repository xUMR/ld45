using UnityEngine;

public class SceneLoaderKeyListener : MonoBehaviour
{
    [SerializeField] private string scene;
    [SerializeField] private KeyCode key;

    private void Update()
    {
        if (Input.GetKeyDown(key))
        {
            Events.ChangeScene(scene);
        }
    }
}
