using System;
using System.Collections;
using UnityEngine;

public class TimeHelper : MonoBehaviour
{
    [SerializeField] private float spellTimeScale;
    [SerializeField] private float delay;
    [SerializeField] private float timeStep;
    [SerializeField] private float timescale;

    private IEnumerator _coroutine;

    private void OnEnable()
    {
        Events.OnCrouch += HandleCrouch;
    }

    private void OnDisable()
    {
        Events.OnCrouch -= HandleCrouch;
    }

    private void Update()
    {
        timescale = Time.timeScale;
    }

    private void HandleCrouch(bool crouched)
    {
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
            Time.timeScale = 1;
        }

        if (!crouched)
        {
            _coroutine = RestoreTimeCoroutine();
            StartCoroutine(_coroutine);
            return;
        }

        _coroutine = SlowTimeCoroutine();
        StartCoroutine(_coroutine);
    }

    private IEnumerator SlowTimeCoroutine()
    {
        var waitUntil = Time.time + delay;
        while (Time.time < waitUntil)
        {
            yield return null;
        }

        while (Time.timeScale > spellTimeScale)
        {
            Time.timeScale -= timeStep;
            yield return null;
        }

        Time.timeScale = spellTimeScale;
    }

    private IEnumerator RestoreTimeCoroutine()
    {
        while (Time.timeScale < 1)
        {
            Time.timeScale += timeStep;
            yield return null;
        }

        Time.timeScale = 1;
    }
}
