using System;
using System.Collections;
using UnityEngine;

public class Wizard : MonoBehaviour
{
    [SerializeField] private float range;
    [SerializeField] private LayerMask enemyLayer;

    [SerializeField] private float areaAttackDelay;

    private RaycastHit2D[] _hits;

    private void OnEnable()
    {
        Events.OnSpellCast += HandleSpellCast;
        _hits = new RaycastHit2D[32];
    }

    private void OnDisable()
    {
        Events.OnSpellCast -= HandleSpellCast;
    }

    private void HandleSpellCast(Spell spell, SpellDirection direction)
    {
        Vector3 offset;
        switch (direction)
        {
            case SpellDirection.Left:
                offset = new Vector3(-spell.Offset.x, spell.Offset.y);
                break;
            case SpellDirection.Area:
                offset = new Vector3(0, spell.Offset.x + .5f);
                break;
            default:
                offset = new Vector3(spell.Offset.x, spell.Offset.y);
                break;
        }

        var rotation = direction == SpellDirection.Area ? Quaternion.Euler(0, 0, 90) : Quaternion.identity;
        var origin = transform.position;
        var spellAttack = Instantiate(spell.Prefab, origin + offset, rotation);

        if (spell.Type == Spell.SpellType.Projectile)
        {
            HandleProjectile(spell, direction, spellAttack, origin);
        }
        else if (spell.Type == Spell.SpellType.Beam)
        {
            HandleBeam(spell, direction, spellAttack, origin);
        }
    }

    private void HandleProjectile(Spell spell, SpellDirection direction, GameObject spellAttack, Vector3 origin)
    {
        var mover = spellAttack.GetComponent<Mover>();

        switch (direction)
        {
            case SpellDirection.Left:
                mover.Reverse();
                break;
            case SpellDirection.Area:
                mover.SendUp();
                AreaCast(spell, origin);
                break;
        }
    }

    private void HandleBeam(Spell spell, SpellDirection direction, GameObject spellAttack, Vector3 origin)
    {
        switch (direction)
        {
            case SpellDirection.Right:

                break;
            case SpellDirection.Left:

                // todo delay
                var localScale = spellAttack.transform.localScale;
                localScale = localScale.WithX(-localScale.x);
                spellAttack.transform.localScale = localScale;

                break;
            case SpellDirection.Area:
                AreaCast(spell, origin);
                break;
        }
    }

    private void AreaCast(Spell spell, Vector2 origin)
    {
        if (spell.Type == Spell.SpellType.Projectile)
        {
            StartCoroutine(ProjectileAreaAttackCoroutine(spell, origin));
        }
        else if (spell.Type == Spell.SpellType.Beam)
        {
            StartCoroutine(BeamAreaAttackCoroutine(spell, origin));
        }
    }

    private IEnumerator BeamAreaAttackCoroutine(Spell spell, Vector2 origin)
    {
//        var waitUntil = Time.time + areaAttackDelay;
//        while (Time.time < waitUntil)
//        {
//            yield return null;
//        }

        var hitsRight = Physics2D.RaycastNonAlloc(origin, Vector2.right, _hits, range, enemyLayer);
        for (var i = 0; i < hitsRight; i++)
        {
            BeamAttackFromAbove(spell, origin, i);
        }

        var hitsLeft = Physics2D.RaycastNonAlloc(origin, Vector2.left, _hits, range, enemyLayer);
        for (var i = 0; i < hitsLeft; i++)
        {
            BeamAttackFromAbove(spell, origin, i);
        }

        yield break;
    }

    private void BeamAttackFromAbove(Spell spell, Vector2 origin, int i)
    {
        var offset = RandomOffsetOffScreenAbove(-3);
        var position = _hits[i].point + offset;

        var spellAttack = Instantiate(spell.Prefab, position, Quaternion.Euler(0, 0, 90));
//        spellAttack.transform.right = (_hits[i].point - origin).normalized;
//        var mover = spellAttack.GetComponent<Mover>();

//        var direction = (_hits[i].point - position).normalized;
//        mover.SetDirection(direction);
    }

    private IEnumerator ProjectileAreaAttackCoroutine(Spell spell, Vector2 origin)
    {
        var waitUntil = Time.time + areaAttackDelay;
        while (Time.time < waitUntil)
        {
            yield return null;
        }

        var hitsRight = Physics2D.RaycastNonAlloc(origin, Vector2.right, _hits, range, enemyLayer);
        for (var i = 0; i < hitsRight; i++)
        {
            ProjectileAttackFromAbove(spell, origin, i);
        }

        var hitsLeft = Physics2D.RaycastNonAlloc(origin, Vector2.left, _hits, range, enemyLayer);
        for (var i = 0; i < hitsLeft; i++)
        {
            ProjectileAttackFromAbove(spell, origin, i);
        }
    }

    private void ProjectileAttackFromAbove(Spell spell, Vector2 origin, int i)
    {
        // todo also look at target? transform.up = ???

        var offset = RandomOffsetOffScreenAbove(7);
        var position = origin + offset;

        var spellAttack = Instantiate(spell.Prefab, position, Quaternion.identity);
        var mover = spellAttack.GetComponent<Mover>();

        var direction = (_hits[i].point - position).normalized;
        mover.SetDirection(direction);
    }

    private static Vector2 RandomOffsetOffScreenAbove(float y)
    {
        var x = UnityEngine.Random.value * 2 - 1;
        return new Vector2(x, y);
    }
}
