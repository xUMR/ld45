using UnityEngine;

public class ChatTrigger : MonoBehaviour
{
    [SerializeField] private string message;
    [SerializeField] private float duration;
    [SerializeField] private int counter;
    [SerializeField] private string expectedTag;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(expectedTag))
        {
            Events.TriggerChat(message, duration);
            counter--;
            if (counter == 0)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
