using System;
using System.Collections;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class WizardChat : MonoBehaviour
{
    [SerializeField] private TMP_Text text;
    [SerializeField] private Image panel;
    [SerializeField] private CanvasGroup canvasGroup;

    #if UNITY_EDITOR
    [SerializeField] private string _message;
    [SerializeField] private float _duration;
    [SerializeField] private float _panelWidth;

    [NaughtyAttributes.Button]
    public void Display()
    {
        Display(_message, _duration, _panelWidth);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            var length = Random.Range(2, 32);
            var stringBuilder = new StringBuilder(length + 1);
            stringBuilder.Append(Helpers.AsciiLettersUpper.Random());
            for (var i = 0; i < length; i++)
            {
                stringBuilder.Append(Helpers.AsciiLettersLower.Random());
                if (Random.value < .1)
                {
                    stringBuilder.Append(' ');
                    i++;
                }
            }

            HandleChat(stringBuilder.ToString(), 1);
        }
    }
    #endif

    private IEnumerator _coroutine;

    private void OnEnable()
    {
        Events.OnChat += HandleChat;
    }

    private void OnDisable()
    {
        Events.OnChat -= HandleChat;
    }

    private void HandleChat(string message, float duration)
    {
        Display(message, duration, DeterminePanelWidth(message.Length + 1));
    }

    private static float DeterminePanelWidth(int messageLength)
    {
        // assume messageLength < 36
        return messageLength * Mathf.Lerp(5.25f, 3.35f, messageLength / 35f);
    }

    public void Display(string message, float duration, float panelWidth)
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);

        _coroutine = DisplayCoroutine(message, duration, panelWidth);
        StartCoroutine(_coroutine);
    }

    private IEnumerator DisplayCoroutine(string message, float duration, float panelWidth)
    {
        var initialColor = text.color;

        panel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, panelWidth);
        canvasGroup.alpha = 0;
        text.text = message;

        var t = 0f;
        var waitUntil = Time.time + 1;
        while (Time.time < waitUntil)
        {
            canvasGroup.alpha = Mathf.Lerp(0f, 1f, Helpers.EaseOutCubic(t));
            t += Time.deltaTime;
            yield return null;
        }

        waitUntil = Time.time + duration;
        while (Time.time < waitUntil)
        {
            yield return null;
        }

        t = 0f;

        waitUntil = Time.time + 1;
        while (Time.time < waitUntil)
        {
            canvasGroup.alpha = Mathf.Lerp(1f, 0f, Helpers.EaseOutCubic(t));
            t += Time.deltaTime;
            yield return null;
        }

        text.text = "";
        text.color = initialColor;
    }
}
