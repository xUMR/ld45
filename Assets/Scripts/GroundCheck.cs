using System;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private float groundDistance;
    [SerializeField] private Rigidbody2D body;

    private RaycastHit2D[] _hits;

    public bool IsOnGround { get; private set; }

    private void UpdateGroundCheck()
    {
//        if (IsOnGround) return;

        var position = body.position;
        Debug.DrawLine(position, position + groundDistance * Vector2.down, Color.magenta);
        var hitCount = Physics2D.RaycastNonAlloc(position, Vector2.down, _hits, groundDistance, layerMask);
        IsOnGround = hitCount > 0;
    }

    private void Awake()
    {
        _hits = new RaycastHit2D[1];
    }

    private void Update()
    {
        UpdateGroundCheck();
    }

    private void FixedUpdate()
    {
        UpdateGroundCheck();
    }

    private void LateUpdate()
    {
//        IsOnGround = false;
    }
}
