using System;
using System.Collections;
using UnityEngine;

public class Follower : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Transform sprite;
    [SerializeField] private float followerDeadZoneDist;
    [SerializeField] private float blinkDist;
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D body;

    [SerializeField] private new SpriteRenderer renderer;
    [SerializeField] private ParticleSystem particlesBlinkIn;
    [SerializeField] private ParticleSystem particlesBlinkOut;
    [SerializeField] private float blinkDuration;

    private WaitForSeconds _waitForBlink;

    private bool _canFollow = true;
    // ReSharper disable once RedundantDefaultMemberInitializer
    private bool _isBlinking = false;

    private float Distance => target.position.x - body.position.x;

    private void Awake()
    {
        _waitForBlink = new WaitForSeconds(blinkDuration);
    }

    private void FixedUpdate()
    {
        if (!_canFollow) return;
        if (_isBlinking) return;

        var distance = Distance;
        var distanceAbs = Mathf.Abs(distance);

        if (distanceAbs > blinkDist)
        {
            Blink();
        }
        else if (distanceAbs > followerDeadZoneDist)
        {
            var direction = distance / distanceAbs * Vector2.right; // (-1, 0) or (1, 0)
            body.velocity = speed * direction;
        }
    }

    private void Update()
    {
        UpdateOrientation();
    }

    private void UpdateOrientation()
    {
        var localScale = sprite.localScale;
        var distance = Distance;

        if (distance < 0)
        {
            localScale = localScale.WithX(-Mathf.Abs(localScale.x));
            sprite.localScale = localScale;
        }
        else
        {
            localScale = localScale.WithX(Mathf.Abs(localScale.x));
            sprite.localScale = localScale;
        }
    }

    private void Blink()
    {
        if (_isBlinking) return;

        StartCoroutine(BlinkCoroutine());
    }

    private IEnumerator BlinkCoroutine()
    {
        _isBlinking = true;

        var initialColor = renderer.color;
        renderer.color = renderer.color.WithA(0);
        particlesBlinkIn.Play();

//        yield return null;
//        yield return new WaitForFixedUpdate();
        yield return WaitFor.FixedUpdate;

        var distance = Distance;
        var xOffset = -distance / Mathf.Abs(distance);
        body.position = target.position.AddXY(xOffset, .25f);

        yield return _waitForBlink;

        particlesBlinkOut.Play();
        renderer.color = initialColor;

        _isBlinking = false;
    }
}
