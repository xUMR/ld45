using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class VignetteController : MonoBehaviour
{
    [SerializeField] private PostProcessVolume volume;

    [SerializeField] private float normal;
    [SerializeField] private float focused;
    [SerializeField] private float step;

    private Vignette _vignette;
    private bool _canFocus;

    private void OnEnable()
    {
        _vignette = volume.profile.GetSetting<Vignette>();

        Events.OnCrouch += HandleFocus;
    }

    private void OnDisable()
    {
        Events.OnCrouch -= HandleFocus;
    }

    private void OnDestroy()
    {
        _vignette.intensity.value = normal;
    }

    private void HandleFocus(bool isFocused)
    {
        _canFocus = isFocused;
    }

    private void Update()
    {
        if (_canFocus && _vignette.intensity < focused)
        {
            _vignette.intensity.value += step;
        }
        else if (!_canFocus && _vignette.intensity > normal)
        {
            _vignette.intensity.value -= step;
        }
    }
}
