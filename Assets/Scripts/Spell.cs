using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Spell", menuName = "MySO/Spell")]
public class Spell : ScriptableObject
{
    [Serializable]
    public enum SpellType
    {
        Projectile,
        Beam
    }

    [SerializeField] private KeyCode[] combo;
    [SerializeField] private string spellName;
    [SerializeField] private float damage;
    [SerializeField] private GameObject prefab;
    [SerializeField] private SpellType type;
    [SerializeField] private Vector3 offset;

    public KeyCode[] Combo => combo;
    public string SpellName => spellName;

    public float Damage => damage;

    public GameObject Prefab => prefab;

    public SpellType Type => type;

    public Vector3 Offset => offset;
}
