using System;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
//    [SerializeField] private EnemyData template;
    [SerializeField] private Transform target;
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody2D body;

    [SerializeField] private float updateFreq;
    [SerializeField] private float distToStopFollow;

    private int _updateCount;
    private float _lastFixedUpdate;

    private bool _canTargetPlayer;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Tags.Player))
        {
            _canTargetPlayer = true;
            target = other.transform.root;
        }
    }

    private void FixedUpdate()
    {
        if (!_canTargetPlayer) return;

        if (Time.time - _lastFixedUpdate < updateFreq) return;
        _updateCount++;
        _lastFixedUpdate = _updateCount * updateFreq;

        var distance = target.position.x - transform.position.x;
        var distanceAbs = Mathf.Abs(distance);

        if (distanceAbs > distToStopFollow)
        {
            _canTargetPlayer = false;
            body.velocity = Vector2.zero;
            return;
        }

        var direction = distance / distanceAbs;

        body.velocity = speed * direction * Vector2.right;
    }
}
